# cv

OEPNCV_VERSION := 4.6.0
OPENCV_URL := $(GITHUB)/opencv/opencv/archive/v$(OPENCV_VERSION).tar.gz

ifdef HAVE_WIN32
DEPS_opencv += winpthreads $(DEPS_winpthreads)
endif

$(TARBALLS)/oepncv-$(OPENCV_VERSION).tar.gz:
	$(call download_pkg,$(OPENCV_URL),oepncv)

.sum-oepncv: oepncv-$(OPENCV_VERSION).tar.gz

oepncv: oepncv-$(OPENCV_VERSION).tar.gz .sum-oepncv
	$(UNPACK)
	$(call pkg_static,opencv-$(OPENCV_VERSION))
	$(MOVE)

OPENCV_CONF := -DBUILD_PKGCONFIG_FILES=ON -DBUILD_CODEC:bool=OFF

.oepncv: oepncv toolchain.cmake
	$(CMAKECLEAN)
	$(HOSTVARS) $(CMAKE) $(OPENCV_CONF)
	+$(CMAKEBUILD)
	$(CMAKEINSTALL)
	touch $@
